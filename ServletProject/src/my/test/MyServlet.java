package my.test;

import model.Employee;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreea.iepure on 11/1/2016.
 */
public class MyServlet extends HttpServlet {


    static volatile  Employee employee = new Employee();
    static volatile String url ;


    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException
    {
        // Set response content type
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        String title = "Using GET Method to Read Form Data";
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " +
                        "transitional//en\">\n";
        out.println(docType +
                "<html>\n" +
                "<head><title>" + title + "</title></head>\n" +
                "<body bgcolor=\"#f0f0f0\">\n" +
                "<h1 align=\"center\">" + title + "</h1>\n" +
                "<ul>\n" +
                "  <li><b>First Name</b>: "
                + employee.getFirstName() + "\n" +
                "  <li><b>Last Name</b>: "
                + employee.getLastName() + "\n" +
                "</ul>\n" +
                "</body></html>");
    }

    // Method to handle POST method request.
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws ServletException, IOException {

        employee.setFirstName(request.getParameter("first_name"));
        employee.setLastName(request.getParameter("last_name"));
        url = "http://localhost:8080/ServletProject/modify.html";
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", url);

    }



    public void destroy()
    {
        // do nothing.
    }
}
